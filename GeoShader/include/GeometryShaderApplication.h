#ifndef __GeometryShaderApplication_H_
#define __GeometryShaderApplication_H_

#include "Application.h"
#include <glm/glm.hpp>

// Derived application class that wraps up all globals neatly
class GeometryShaderApplication : public Application
{
public:

	GeometryShaderApplication();
	virtual ~GeometryShaderApplication();

protected:

	virtual bool onCreate();
	virtual void Update(float a_deltaTime);
	virtual void Draw();
	virtual void Destroy();

	//function to load in a texture
	unsigned int LoadTexture(const char* a_textureFilename);

	struct SVertex
	{
		glm::vec4 pos;
		glm::vec4 colour;
	};
	
	glm::mat4	m_cameraMatrix;
	glm::mat4	m_projectionMatrix;

	unsigned int	m_programID;
	unsigned int	m_vertexShader;
	unsigned int	m_geometryShader;
	unsigned int	m_fragmentShader;

	unsigned int m_maxIndices;
	unsigned int m_vao;
	unsigned int m_vbo;
	unsigned int m_ibo;

	//Texture Handle for OpenGL
	unsigned int m_textureID;

	SVertex* m_vertices;

	int		m_currentState;
};

#endif // __GLTextureApplication_H_