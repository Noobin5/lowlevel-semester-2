#version 150

in vec4 Position; 
in vec4 Colour;

out Vertex
{
	vec4 Colour;
} vertex;


void main() 
{ 
	vertex.Colour = Colour;
	gl_Position = Position; 
}