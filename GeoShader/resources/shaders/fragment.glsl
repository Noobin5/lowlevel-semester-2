#version 150

in vec4 vColour;
in vec2 vUV;

out vec4 FragColor;

uniform sampler2D textureLoc;
 
void main()
{
	FragColor =  vColour * texture2D( textureLoc, vUV).a; 
}
