#version 150

layout(points) in;
layout(triangle_strip, max_vertices = 4) out;


in Vertex
{
	vec4 Colour;
}vertex[];

out vec4 vColour;
out vec2 vUV;

uniform mat4 ProjectionView; 
uniform vec4 cameraPosition;

void main( )
{
	vec3 P = gl_in[0].gl_Position.xyz;

	mat3 billboard;
	billboard[2] = normalize( cameraPosition.xyz - P);
	billboard[0] = cross( vec3(0,1,0), billboard[2] );
	billboard[1] = cross( billboard[2], billboard[0]);

	float halfSize = 2 * 0.5f;
	
	vec3 corners[4];
	corners[0] = P + billboard * vec3( -halfSize, -halfSize, 0 );
	corners[1] = P + billboard * vec3( halfSize, -halfSize, 0 );
	corners[2] = P + billboard * vec3( -halfSize, halfSize, 0 );
	corners[3] = P + billboard * vec3( halfSize, halfSize, 0 );

	
	
	vec2 UVCoords [4];
	UVCoords[0] = vec2( 0.f, 0.f);
	UVCoords[1] = vec2( 1.f, 0.f);
	UVCoords[2] = vec2( 0.f, 1.f);
	UVCoords[3] = vec2( 1.f, 1.f);

	for ( int i = 0 ; i < 4 ; ++i )
	{
		vColour = vertex[0].Colour;
		vUV = UVCoords[i];
		gl_Position = ProjectionView * vec4( corners[ i ], 1 );
		EmitVertex();
	}
	//EndPrimitive();
}