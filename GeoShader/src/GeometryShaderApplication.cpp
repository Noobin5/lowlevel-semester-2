#include "GeometryShaderApplication.h"
#include "Gizmos.h"
#include "Utilities.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/ext.hpp>
#include <SOIL.h>
#include <iostream>


//\=======================================================
// This sample projects demonstrates how to create and 
// manipulate vertex and index buffers in OpenGL
//\=======================================================

#define DEFAULT_SCREENWIDTH 1280
#define DEFAULT_SCREENHEIGHT 720


GeometryShaderApplication::GeometryShaderApplication()
{

}

GeometryShaderApplication::~GeometryShaderApplication()
{

}

bool GeometryShaderApplication::onCreate()
{
	m_currentState = 0; //state 0 is draw a triangle state 1 is draw a box
	// initialise the Gizmos helper class
	Gizmos::create();
	//limit the maximum vertices we can have
	m_maxIndices = 4;
	//Load the shaders for this program
	m_vertexShader = Utility::loadShader("./shaders/vertex.glsl", GL_VERTEX_SHADER);
	m_fragmentShader = Utility::loadShader("./shaders/fragment.glsl", GL_FRAGMENT_SHADER);
	m_geometryShader = Utility::loadShader("./shaders/geometry.glsl", GL_GEOMETRY_SHADER);
	//Define the input and output varialbes in the shaders
	//Note: these names are taken from the glsl files -- added in inputs for UV coordinates
	const char* szInputs[] = { "Position", "Colour" };
	const char* szOutputs[] = { "FragColor" };
	//bind the shaders to create our shader program
	m_programID = Utility::createProgram(
		m_vertexShader, 
		0,
		0,
		m_geometryShader,
		m_fragmentShader,
		2, szInputs, 1, szOutputs);

	m_vertices = new SVertex[4];

	//Generate and bind our Vertex Array Object
	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);

	//Generate our Vertex Buffer Object
	glGenBuffers(1, &m_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(SVertex), m_vertices, GL_DYNAMIC_DRAW);
	//Define our vertex attribute data for this buffer we have to do this for our vertex input data
	//Position, Normal & Colour this attribute order is taken from our szInputs string array order
	glEnableVertexAttribArray(0); //position
	glEnableVertexAttribArray(1); //colour
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(SVertex), 0);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_TRUE, sizeof(SVertex), ((char*)0) + 16);
	
	//generate and bind an index buffer
	glGenBuffers(1, &m_ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
	//the thrid argument of glBufferData is the data that will fill this buffer here it is set to nullptr
	//so this buffer will just have empty data in it
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_maxIndices * sizeof(unsigned int), nullptr, GL_STATIC_DRAW);

	glBindVertexArray(0);
	//unbind our current vertex Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//unbind our current index buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	//Set our vertex and Index buffers in here as they don't get altered for this programs run through
	//set the vertices for our billboard -- there is no reason this couldn't be in the init function as it does not 

	m_vertices[0].pos = glm::vec4(-4.f, 0.f, 0.f, 1.f);
	m_vertices[0].colour = glm::vec4(1.f, 0.f, 0.f, 1.f);
	
	m_vertices[1].pos = glm::vec4(4.f, 0.f, 0.f, 1.f);
	m_vertices[1].colour = glm::vec4(0.f, 1.f, 0.f, 1.f);
	
	m_vertices[2].pos = glm::vec4(-4.f, 4.f, 0.f, 1.f);
	m_vertices[2].colour = glm::vec4(0.f, 0.f, 1.f, 1.f);
	
	m_vertices[3].pos = glm::vec4(4.f, 4.f, 0.f, 1.f);
	m_vertices[3].colour = glm::vec4(1.f, 1.f, 1.f, 1.f);
	
	//bind our vertex buffer and fill it with our mertex data
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(SVertex), m_vertices, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//bind our index buffer and specify the indices order in the indices array		
	unsigned int indices[] = { 0, 1, 2, 3 };
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_maxIndices * sizeof(unsigned int), indices, GL_STATIC_DRAW);

	//see if there are any errors on the GL side
	Utility::GetGLErrors();

	//load in our Texture
	m_textureID = LoadTexture("./images/starAlpha.png");

	// create a world-space matrix for a camera
	m_cameraMatrix = glm::inverse( glm::lookAt(glm::vec3(10,10,10),glm::vec3(0,0,0), glm::vec3(0,1,0)) );
	
	// create a perspective projection matrix with a 90 degree field-of-view and widescreen aspect ratio
	m_projectionMatrix = glm::perspective(glm::pi<float>() * 0.25f, DEFAULT_SCREENWIDTH/(float)DEFAULT_SCREENHEIGHT, 0.1f, 1000.0f);

	// set the clear colour and enable depth testing and backface culling
	glClearColor(0.25f,0.25f,0.25f,1.f);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glEnable(GL_BLEND);
	return true;
}

void GeometryShaderApplication::Update(float a_deltaTime)
{
	// update our camera matrix using the keyboard/mouse
	Utility::freeMovement( m_cameraMatrix, a_deltaTime, 10 );
	//Get some user input to set the current drawing state
	GLFWwindow* window = glfwGetCurrentContext();


	// clear all gizmos from last frame
	Gizmos::clear();
	
	// add an identity matrix gizmo
	Gizmos::addTransform( glm::mat4(1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1) );
	// add a 20x20 grid on the XZ-plane
	for ( int i = 0 ; i < 21 ; ++i )
	{
		Gizmos::addLine( glm::vec3(-10 + i, 0, 10), glm::vec3(-10 + i, 0, -10), 
						 i == 10 ? glm::vec4(1,1,1,1) : glm::vec4(0,0,0,1) );
		
		Gizmos::addLine( glm::vec3(10, 0, -10 + i), glm::vec3(-10, 0, -10 + i), 
						 i == 10 ? glm::vec4(1,1,1,1) : glm::vec4(0,0,0,1) );
	}

	

	
	// quit our application when escape is pressed
	if (glfwGetKey(m_window,GLFW_KEY_ESCAPE) == GLFW_PRESS)
		quit();
}

void GeometryShaderApplication::Draw()
{
	// clear the backbuffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	// get the view matrix from the world-space camera matrix
	glm::mat4 viewMatrix = glm::inverse( m_cameraMatrix );
	// draw the gizmos from this frame
	Gizmos::draw(viewMatrix, m_projectionMatrix);

	//bing our shader program
	glUseProgram(m_programID);
	//bind our vertex array object
	glBindVertexArray(m_vao);
	

	//get our shaders uniform location for our projectionViewMatrix and then use glUniformMatrix4fv to fill it with the correct data
	unsigned int projectionViewUniform = glGetUniformLocation(m_programID, "ProjectionView");
	glUniformMatrix4fv(projectionViewUniform, 1, false, glm::value_ptr(m_projectionMatrix * viewMatrix));

	unsigned int cameraPosUniform = glGetUniformLocation(m_programID, "cameraPosition");
	glUniform4fv(cameraPosUniform, 1, glm::value_ptr(m_cameraMatrix[3]));

	//bind our textureLocation variable from the shaders and set it's value to 0 as the active texture is texture 0
	unsigned int texUniformID = glGetUniformLocation(m_programID, "textureLoc");
	glUniform1i(texUniformID, 0);
	//set our active texture, and bind our loaded texture
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_textureID);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//depending on the state call draw with glDrawElements to draw our buffer
	//glDrawElements uses the index array in our index buffer to draw the vertices in our vertex buffer
	glDrawElements(GL_POINTS, m_maxIndices, GL_UNSIGNED_INT, 0);

	glBindVertexArray(0);
	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	//glBindBuffer(GL_ARRAY_BUFFER, 0);
	glUseProgram(0);
	
	//call to GLGetErrors in case any errors arise
	Utility::GetGLErrors();


}

void GeometryShaderApplication::Destroy()
{
	//clean up
	Gizmos::destroy();
	//if we generate it destroy it
	glDeleteTextures(1, &m_textureID);
	glDeleteBuffers(1, &m_vbo);
	glDeleteBuffers(1, &m_ibo);
	glDeleteVertexArrays(1, &m_vao);
	glDeleteProgram(m_programID);
	glDeleteShader(m_fragmentShader);
	glDeleteShader(m_vertexShader);
}

//simple function to load in a GL texture using SOIL
unsigned int GeometryShaderApplication::LoadTexture(const char* a_textureFilename)
{
	unsigned int texID = 0;
	if (a_textureFilename != nullptr)
	{
		int width = 0; int height = 0; int bitsPerPixel = 0;
		unsigned char* pRawImageData = SOIL_load_image(a_textureFilename, (int*)&width, (int*)&height, (int*)&bitsPerPixel, SOIL_LOAD_AUTO);
		if (pRawImageData)
		{
			texID = SOIL_create_OGL_texture(pRawImageData,
				width,
				height,
				bitsPerPixel,
				SOIL_CREATE_NEW_ID,
				SOIL_FLAG_MIPMAPS |
				SOIL_FLAG_INVERT_Y |
				SOIL_FLAG_NTSC_SAFE_RGB |
				SOIL_FLAG_COMPRESS_TO_DXT);
			SOIL_free_image_data(pRawImageData);
		}
	}
	if (texID == 0)
	{
		std::cout << "SOIL Image unable to load: " << a_textureFilename << std::endl;
	}
	return texID;
}

